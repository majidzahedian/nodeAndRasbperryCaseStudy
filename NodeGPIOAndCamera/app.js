var rpio = require('rpio');

const Raspistill = require('node-raspistill').Raspistill;
const fs = require('fs');
var isReadyForTakeImage=true;

const uuidv1 = require('uuid/v1');

const raspistill = new Raspistill({
    noFileSave: true,
    encoding: 'jpg',
	width: 800,
    height: 600
});

rpio.open(22, rpio.INPUT, rpio.PULL_UP);

function pollcb(pin)
{
        
        var state = rpio.read(pin);
        console.log('Button event on P%d (button currently %d)', pin, state);
	if(state==1)
	{
	isReadyForTakeImage=false;
	
	raspistill.takePhoto()
	    .then((photo) => {
	        console.log('took photo');
	        fs.writeFile(uuidv1()+'.jpg', photo, {encoding: 'binary'}, function (err) {
	            if (err) {
	                isReadyForTakeImage=true;
	            }
	
	            isReadyForTakeImage=true;
	        })
	    })
	    .catch((error) => {
	        console.error('something bad happened', error);
	    });
	
	
	}

}

rpio.poll(22, pollcb);
