var fs = require('fs');
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  var obj;
  fs.readFile('db.json', 'utf8', function (err, data) {
    if (err) throw err;
    obj = JSON.parse(data);
    console.log(obj);
    res.render('index', { title:'Camera Image and Time' , pics: obj.pictures });

  })


});

module.exports = router;
