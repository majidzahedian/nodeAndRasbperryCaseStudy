var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var rpio = require('rpio');

const Raspistill = require('node-raspistill').Raspistill;
const fs = require('fs');
var isReadyForTakeImage=true;

const uuidv1 = require('uuid/v1');

const raspistill = new Raspistill({
    noFileSave: true,
    encoding: 'jpg',
	width: 800,
    height: 600
});


var index = require('./routes/index');
var users = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

rpio.open(22, rpio.INPUT, rpio.PULL_UP);
function pollcb(pin)
{
        
    var state = rpio.read(pin);

	if(state==1)
	{
	isReadyForTakeImage=false;
	
	raspistill.takePhoto()
	    .then((photo) => {
	        console.log('took photo');
	        var fileName='takedImages/'+uuidv1()+'.jpg';
	        fs.writeFile('public/'+fileName, photo, {encoding: 'binary'}, function (err) {

	            if (err) {
	                isReadyForTakeImage=true;
	            }
	
				var obj;
				fs.readFile('db.json', 'utf8', function (err, data) {
				if (err) throw err;
				obj = JSON.parse(data);
				obj.pictures[obj.pictures.length]={"Time": new Date().toISOString(),"URL":fileName};
				fs.writeFile('db.json',JSON.stringify(obj), 'utf8', function() {});
				
	            isReadyForTakeImage=true;

	        })
	    })
	    .catch((error) => {
	        console.error('something bad happened', error);
	    });
	
	
	})
}

}

rpio.poll(22, pollcb);


module.exports = app;